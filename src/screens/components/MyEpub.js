import React from 'react'
import { Epub } from 'epubjs-rn'
import {View} from 'react-native'
import {Spinner} from 'native-base' 

export default class App extends React.Component { 
  spinner(){
    if(this.state.isLoading)
      return <Spinner/>
  }
  state = {
    isLoading: true
  }
  render() {
    const resourceType = 'url' 
    console.log("loading",this.props.url)
    return (
      <View style={{ flex: 1 }}>
        {this.spinner()} 
        <Epub 
            style={{ flex: 1 }}
            src={this.props.url} 
            flow={"paginated"}
            onReady={() => {
              console.log("loaded")
              this.setState( previousState => (
                { isLoading: false }
              ))
            }} 
          />       
      </View>
    );
  }
}