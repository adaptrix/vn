// With Flow type annotations (https://flow.org/)
import React from 'react'
import PDFView from 'react-native-view-pdf';
import {View} from 'react-native'
import {Spinner} from 'native-base'
// Without Flow type annotations
// import PDFView from 'react-native-view-pdf/lib/index'; 

export default class App extends React.Component { 
  spinner(){
    if(this.state.isLoading)
      return <Spinner/>
  }
  state = {
    isLoading: true
  }
  render() {
    const resourceType = 'url' 
    console.log("loading",this.props.url)
    return (
      <View style={{ flex: 1 }}>
        {this.spinner()}
        <PDFView
            fadeInDuration={250.0}
            style={{ flex: 1 }}
            resource={this.props.url}
            resourceType={this.props.type}
            onLoad={() => {
              console.log("loaded")
              this.setState( previousState => (
                { isLoading: false }
              ))
            }}
            onError={error => { 
              console.log('Cannot render PDF', error)
              this.setState( previousState => (
                { isLoading: false }
              ))
            }}
          />       
      </View>
    );
  }
}