import RNFetchBlob from "rn-fetch-blob"
import env from "../environments/app" 
import {Platform} from "react-native"

const fileUrlSuffix = (Platform.OS === 'android') ? 'file://' : ''

let dirs = RNFetchBlob.fs.dirs
let task
let textBookName
let storageDirectory = dirs.CacheDir
let storageDirectoryWithSuffix = fileUrlSuffix + dirs.CacheDir


function getDirectories(){
    return RNFetchBlob.fs.dirs
}

function getFilesInDirectory(directory,filesFn){
    RNFetchBlob.fs.ls(storageDirectory+directory).then(filesFn).catch(e=>console.log(e))
}

function cancelDownload(cancelFn){ 
    task.cancel(err=>{
        RNFetchBlob.fs.unlink(textBookName).then(cancelFn).catch(e=>console.log(e))
    })
}

function deleteFile(fileName,deleteFn){
    RNFetchBlob.fs.unlink(storageDirectory + fileName).then(deleteFn).catch(e=>console.log(e))
}

function downloadFile(url,name,progressFn,completedFn){
    url = env.resourcesUrl + url
    textBookName = storageDirectory + name 
    console.log(textBookName)
    task = RNFetchBlob
    .config({ 
        path : textBookName,
        fileCache : true
    })
    .fetch('GET', url, {
        //some headers ..
    })
    task.progress(progressFn)
    task.then(completedFn) 
    task.catch(error=>console.log(error))
}

export default{
    getDirectories,
    downloadFile,
    cancelDownload,
    getFilesInDirectory,
    storageDirectory,
    storageDirectoryWithSuffix,
    fileUrlSuffix,
    deleteFile
}