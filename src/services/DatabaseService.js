
import 'es6-symbol/implement'  
import { Database } from '@nozbe/watermelondb'
import SQLiteAdapter from '@nozbe/watermelondb/adapters/sqlite'
import schema from '../models/schema'
import TextBook from '../models/TextBook'
import ReferenceBook from '../models/ReferenceBook'
import LiteratureBook from '../models/LiteratureBook'

let database

function initiate(){    
    const adapter = new SQLiteAdapter({
        schema,
    }) 

    database = new Database({
        adapter,
        modelClasses: [
            TextBook,
            LiteratureBook,
            ReferenceBook
        ],
        actionsEnabled: true,
    })
}

function saveTexbook(textBook){
    const textBookCollection = database.collection.get('text_books')
}

export default{
    initiate
}