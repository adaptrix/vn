import axios from 'axios'
import env from '../environments/app'
import StorageService from "./StorageService"
 
let apiUrl = env.apiUrl
 

function log(val){
    console.log(val)
    alert(val)
} 

function request(method, url, body, callback){
    url = apiUrl+url 
    fetch(url, {
        method: method,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    }).then((response)=>response.json()).then(callback).catch(log)
}

function secureRequest(method, url, body, params, callback){
    let queryString = ""
    let index = 0
    if(params){
        for(a in params){
            const element = params[a];
            let query = index===0?`?${a}=${element}`:`&${a}=${element}`
            queryString = queryString + query
            index++
        } 
    }

    url = apiUrl+url+queryString
 
    StorageService.getAuthSession(authSession=>{
        if(authSession.api_token)
            if(method!=="GET")
                fetch(url, {
                    method: method,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${authSession.api_token}`
                    },
                    body: JSON.stringify(body),
                }).then((response)=>response.json()).then(callback).catch(log)
            else
                fetch(url, {
                    method: method,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${authSession.api_token}`
                    }
                }).then((response)=>response.json()).then(callback).catch(log)
        else
            callback("failed")
    })
 
}

function login({login,password},callback){
    request('POST','login',{login:login, password:password},callback);
}

function getAllTextBooks(params,callback){
    secureRequest('GET', 'text_books', null, params, callback)
}

function getAllReferenceBooks(params,callback){
    secureRequest('GET', 'reference_books', null, params, callback)
}


function getAllLiteratureBooks(params,callback){
    secureRequest('GET', 'literature_books', null, params, callback)
}


export default { 
    login,
    getAllTextBooks,
    getAllLiteratureBooks,
    getAllReferenceBooks
}