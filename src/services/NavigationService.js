import { NavigationActions, DrawerActions } from 'vue-native-router';

let _navigator;
let _drawer;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function setDrawerNavigator(drawerRef){
  _drawer = drawerRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

function openDrawer(){ 
  _drawer.dispatch(
    DrawerActions.openDrawer()
  );
} 

function addEventListener(name, callback){
  this._navigator.addListener(name,callback)
}

function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
} 

export default {
  navigate,
  setTopLevelNavigator, 
  setDrawerNavigator,
  addEventListener,
  getActiveRouteName,
  openDrawer
};