import AsyncStorage from "@react-native-community/async-storage"
import env from "../environments/app" 
import FileSystemService from "./FileSystemService"
import { Platform } from "react-native"

const get = async (key) => {
    try {
        const value = await AsyncStorage.getItem(key)
        return value
    } catch(e) {
        console.log(e)
        return null
    }
}

const set = async (key, val) => {
    try {
        val = JSON.stringify(val)
        await AsyncStorage.setItem(key, val)
    } catch (e) {
        console.log(e)
    }
}

const destroy = async (key) => {
    try {
      return await AsyncStorage.removeItem(key)
    } catch(e) {
        console.log(e)
    } 
}

function setAuthSession(val){
    set("authSession",val)
}

function storeTextBook(textBook){
    textBook.thumbnail_url = FileSystemService.storageDirectoryWithSuffix + env.tbThumbnailStorageLocation + textBook.id
    textBook.url = FileSystemService.storageDirectoryWithSuffix + env.textbookStorageLocation + textBook.id
    textBook.local = true
    console.log(textBook)
    set("tb"+textBook.id,textBook)
}

function storeReferenceBook(referenceBook){
    referenceBook.thumbnail_url = FileSystemService.storageDirectoryWithSuffix + env.rbThumbnailStorageLocation + referenceBook.id
    referenceBook.url = FileSystemService.storageDirectoryWithSuffix + env.referencebookStorageLocation + referenceBook.id
    referenceBook.local = true
    console.log(referenceBook)
    set("tb"+referenceBook.id,referenceBook)
}

function storeLiteratureBook(literatureBook){
    literatureBook.thumbnail_url = FileSystemService.storageDirectoryWithSuffix + env.lbThumbnailStorageLocation + literatureBook.id
    literatureBook.url = FileSystemService.storageDirectoryWithSuffix + env.literaturebookStorageLocation + literatureBook.id
    literatureBook.local = true
    console.log(literatureBook)
    set("lb"+literatureBook.id,literatureBook)
}

function getTextBook(textBook_id,callback){
    get("tb"+textBook_id).then(response=>JSON.parse(response)).then(callback)
}

function getLiteratureBook(literatureBook_id,callback){
    get("lb"+literatureBook_id).then(response=>JSON.parse(response)).then(callback)
}

function getAuthSession(callback){
    get("authSession").then(response=>JSON.parse(response)).then(callback)
}

function destroyAuthSession(callback=()=>null){
    destroy("authSession").then(callback)
} 

function destroyTextBook(textBook_id,callback) {
    destroy("tb"+textBook_id).then(callback)
}

function destroyLiteratureBook(literatureBook_id,callback) {
    destroy("lb"+literatureBook_id).then(callback)
}

async function getAllKeys(){ 
    let keys = []
    try {
        keys = await AsyncStorage.getAllKeys()
    } catch(e) { 
        console.log(e)
    }
    
    console.log(keys) 
}

export default{
    getAuthSession,
    setAuthSession,
    destroyAuthSession,
    storeTextBook,
    storeReferenceBook,
    storeLiteratureBook,
    getTextBook,
    getLiteratureBook,
    getAllKeys,
    destroyTextBook,
    destroyLiteratureBook
}