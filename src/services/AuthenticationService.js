import RequestsService from "./RequestsService"
import StorageService from "./StorageService"

function checkAuthentication(callback,failed){
   StorageService.getAuthSession(authSession=>{ 
        authSession?callback(authSession):failed()
   })
}

function login(authenticationCredentials,callback,failed){
    RequestsService.login(authenticationCredentials,res=>{  
        if(res.success){ 
            StorageService.setAuthSession(res.response)
            callback(res.response)
        }
        else
            failed(res.message)
    })
}

function logout(callback){
    StorageService.destroyAuthSession(callback)
}

export default{
    checkAuthentication,
    login,
    logout
}
