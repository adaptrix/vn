export default {
    apiUrl: "https://library.tie.sc.tz/api/",
    homeUrl: "https://library.tie.sc.tz/",
    resourcesUrl: "https://library.tie.sc.tz/storage/",
    verificationUrl: "https://library.tie.sc.tz/account",
    verificationSuccessUrl: "https://library.tie.sc.tz/account/registration",
    passwordResetUrl: "https://library.tie.sc.tz/password/reset",
    textbookStorageLocation: "/tsl/",
    referencebookStorageLocation: "/rsl/",
    literaturebookStorageLocation: "/lsl/",
    tbThumbnailStorageLocation: "/tbsl/",
    rbThumbnailStorageLocation: "/rtsl/",
    lbThumbnailStorageLocation: "/lbsl/"
}