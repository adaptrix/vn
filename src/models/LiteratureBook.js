// model/Post.js
import { Model } from '@nozbe/watermelondb'
import { field } from '@nozbe/watermelondb/decorators'

export default class LiteratureBook extends Model {
  static table = 'text_books' 
  @field('book_id') bookId
  @field('title') title
  @field('genre') genre
  @field('url') url
  @field('publisher') publisher
  @field('language') language
  @field('description') description
  @field('country') country
  @field('thumbnail_url') thumbnail_url
  @field('year') year
  @field('size') size
}