import { appSchema, tableSchema } from '@nozbe/watermelondb'

export default appSchema({
  version: 1,
  tables: [
      tableSchema({
      name: 'text_books',
      columns: [ 
          { name: 'book_id', type: 'string', isOptional: true }, 
          { name: 'title', type: 'string', isOptional: true }, 
          { name: 'subject', type: 'string', isOptional: true }, 
          { name: 'url', type: 'string', isOptional: true }, 
          { name: 'publisher', type: 'string', isOptional: true }, 
          { name: 'language', type: 'string', isOptional: true }, 
          { name: 'description', type: 'string', isOptional: true }, 
          { name: 'country', type: 'string', isOptional: true }, 
          { name: 'thumbnail_url', type: 'string', isOptional: true }, 
          { name: 'year', type: 'string', isOptional: true }, 
          { name: 'size', type: 'string', isOptional: true }, 
      ]
      }), 
      tableSchema({
      name: 'reference_books',
      columns: [ 
          { name: 'book_id', type: 'string', isOptional: true }, 
          { name: 'title', type: 'string', isOptional: true }, 
          { name: 'subject', type: 'string', isOptional: true }, 
          { name: 'url', type: 'string', isOptional: true }, 
          { name: 'publisher', type: 'string', isOptional: true }, 
          { name: 'language', type: 'string', isOptional: true }, 
          { name: 'description', type: 'string', isOptional: true }, 
          { name: 'country', type: 'string', isOptional: true }, 
          { name: 'thumbnail_url', type: 'string', isOptional: true }, 
          { name: 'year', type: 'string', isOptional: true }, 
          { name: 'size', type: 'string', isOptional: true }, 
      ]
      }), 
      tableSchema({
      name: 'literature_books',
      columns: [ 
          { name: 'book_id', type: 'string', isOptional: true }, 
          { name: 'title', type: 'string', isOptional: true }, 
          { name: 'genre', type: 'string', isOptional: true }, 
          { name: 'url', type: 'string', isOptional: true }, 
          { name: 'publisher', type: 'string', isOptional: true }, 
          { name: 'language', type: 'string', isOptional: true }, 
          { name: 'description', type: 'string', isOptional: true }, 
          { name: 'country', type: 'string', isOptional: true }, 
          { name: 'thumbnail_url', type: 'string', isOptional: true }, 
          { name: 'year', type: 'string', isOptional: true }, 
          { name: 'size', type: 'string', isOptional: true }, 
      ]
      }), 
  ],
})